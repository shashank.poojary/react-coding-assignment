
# React Coding Challenge

## Overview

Building upon this skeleton project, implement a Contact Management Portal UI using React js and redux.
## Getting Started

 - Clone this repository
 - Install dependencies `npm install`
 - Run the development server `npm start`

## Criteria

 - As a user, I should be able to add a new contact 
	 - Name (first and last) (max 100 char) required
   	 -  Number (standard validation and user can have multiple phone numbers) required
   	 - Type of number (Eg. company, personal, each number can have different type)    
   	 - email 
	 - Notes
 - As a user, I should be able to Edit and delete any contact.
	
 - As a user, I should able to mark any of contact as important. 
 - As a user, I should able to list all the contacts 
		- filter by important contact.
		- sort by name.
 - As a user, I should be able to archive an of the contact.
 - As a user, I should be able to see all archived contacts and restore them.

## Things to think about

- Re-usable components
- Redux structure for storing the data. 
- Code should be ESLint compatible. 

## UI

You have full creative freedom with the design of the solution.

*The design of the final solution is considered **less important** than the full coverage of the above criteria.*

## Submitting the Solution

You create a branch and create an MR when you complete this task.

